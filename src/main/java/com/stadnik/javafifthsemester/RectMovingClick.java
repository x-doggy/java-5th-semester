package com.stadnik.javafifthsemester;

/*
 * 6. Направление движения прямоугольника по экрану изменяется щелчком по клавише мыши случайным образом.
 *    При этом каждый второй щелчок меняет цвет фона.
 */

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Arc2D;
import javax.swing.*;

public class RectMovingClick {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            RectMovingFrame frame = new RectMovingFrame();
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}

class RectMovingFrame extends JFrame {
    protected final static int DEFAULT_WIDTH  = 500;
    protected final static int DEFAULT_HEIGHT = 500;

    RectMovingFrame() {
        setTitle("Прямоугольник движется и меняется по щелчку");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        getContentPane().add(new RectMovingPanel());
    }
}

class RectMovingPanel extends JPanel implements Runnable {
    private static final int RECT_WIDTH = 100;
    private static final int RECT_HEIGHT = 40;
    private int posX = 0, posY = 0;
    private int dirX = 1, dirY = 1;
    private boolean isFirstClick = false;

    RectMovingPanel() {
        Thread thread = new Thread(this);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (!isFirstClick) {
                    dirX = (int) (Math.random() * 20 - 10);
                    dirY = (int) (Math.random() * 20 - 10);
                } else {
                    setBackground(new Color(
                            (int) (Math.random() * 255),
                            (int) (Math.random() * 255),
                            (int) (Math.random() * 255)
                    ));
                }
                isFirstClick = !isFirstClick;
                repaint();
            }
        });
        thread.start();
        setFocusable(true);
    }

    @Override
    public void paintComponent(Graphics h) {
        super.paintComponent(h);
        Graphics2D g = (Graphics2D) h;
        g.drawRect(posX, posY, RECT_WIDTH, RECT_HEIGHT);
        Arc2D.Double a = new Arc2D.Double();
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(100);
                repaint();
                posX += dirX;
                posY += dirY;
                if (posX > getWidth() || posX <= 0)
                    posX = 0;
                if (posY > getHeight() || posY <= 0)
                    posY = 0;
                repaint();
            } catch (InterruptedException e) {
            }
        }
    }
}

