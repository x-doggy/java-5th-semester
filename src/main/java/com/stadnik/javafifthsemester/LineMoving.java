package com.stadnik.javafifthsemester;

/*
 * 8. Клавиши "вверх", "вниз", "вправо", "влево" двигают в соответствующем
 *    направлении линию. При достижении границ апплета линия появляется с
 *    противоположной стороны.
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import static java.awt.event.KeyEvent.*;

public class LineMoving {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            LineMovingFrame frame = new LineMovingFrame();
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}

class LineMovingFrame extends JFrame {
    protected final static int DEFAULT_WIDTH  = 600;
    protected final static int DEFAULT_HEIGHT = 600;

    LineMovingFrame() {
        setTitle("Клавиши двигают линию");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        getContentPane().add(new LineMovingPanel());
    }
}

class LineMovingPanel extends JPanel {
    private int x1, y1, x2, y2;
    private static final int STEP = 5;  // Animation step
    private static final int DIFF = 80; // Line second point pos

    LineMovingPanel() {
        x1 = 10;
        y1 = 10;
        x2 = x1 + DIFF;
        y2 = y1 + DIFF;

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case VK_UP:
                        y1 -= STEP;
                        y2 -= STEP;
                        break;
                    case VK_DOWN:
                        y1 += STEP;
                        y2 += STEP;
                        break;
                    case VK_LEFT:
                        x1 -= STEP;
                        x2 -= STEP;
                        break;
                    case VK_RIGHT:
                        x1 += STEP;
                        x2 += STEP;
                        break;
                }
                checkNearSides();
                repaint();
            }

            @Override
            public void keyReleased(KeyEvent e) {
                repaint();
            }
        });

        setFocusable(true);
    }

    private void checkNearSides() {
        if (x2 <= 0) {
            x2 = getWidth() - 1;
            x1 = x2 - DIFF;
        }
        else if (x1 >= getWidth()) {
            x1 = 0;
            x2 = x1 + DIFF;
        }
        else if (y2 <= 0) {
            y2 = getHeight() - 1;
            y1 = y2 - DIFF;
        }
        else if (y1 >= getHeight()) {
            y1 = 0;
            y2 = y1 + DIFF;
        }
    }

    @Override
    public void paintComponent(Graphics h) {
        Graphics2D g = (Graphics2D) h;
        g.clearRect(0, 0, getWidth(), getHeight());
        g.setStroke(new BasicStroke(4));
        g.setColor(new Color(0, 0, 153));
        g.drawLine(x1, y1, x2, y2);
    }
}
