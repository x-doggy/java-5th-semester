package com.stadnik.javafifthsemester;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/*
 * 1. Создать форму с несколькими кнопками так, чтобы надпись на первой
 *    кнопке при ее нажатии передавалась на следующую, и т.д.
 */

public class MyButton {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            MyButtonFrame frame = new MyButtonFrame();
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}

class MyButtonFrame extends JFrame {
    protected final static int BUTTON_NUMBER = 10;
    protected final JButton[] buttons = new JButton[BUTTON_NUMBER];

    MyButtonFrame() {
        setTitle("Button frame test");
        setSize(500, 120);

        Container c = getContentPane();
        c.setLayout(new FlowLayout());

        for (int i = 0; i < BUTTON_NUMBER; i++) {
            final JButton localButton = new JButton("Button" + Integer.toString(i));
            localButton.addActionListener(new MoveTextAction());
            buttons[i] = localButton;
            c.add(localButton);
        }
    }

    private class MoveTextAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            final String tmp = buttons[0].getText();
            for (int i = BUTTON_NUMBER - 1; i > 0; i--) {
                final String s = buttons[i].getText();
                buttons[i].setText(buttons[i-1].getText());
                buttons[i - 1].setText(s);
            }
            buttons[0].setText(buttons[BUTTON_NUMBER - 1].getText());
            buttons[BUTTON_NUMBER - 1].setText(tmp);
        }
    }
}
