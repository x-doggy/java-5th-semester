package com.stadnik.javafifthsemester;

/*
 * 10. Квадрат на экране движется к указателю мыши,
 *     когда последний находится в границах апплета.
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SquareMouseMoving {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            SquareFrame frame = new SquareFrame();
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}

class SquareFrame extends JFrame {
    protected final static int DEFAULT_WIDTH  = 500;
    protected final static int DEFAULT_HEIGHT = 500;

    SquareFrame() {
        setTitle("Квадрат движется к указателю мыши");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        getContentPane().add(new SquarePanel());
    }
}

class SquarePanel extends JPanel {
    // Сторона квадрата
    private static final int SQUARE_SIZE = 40;
    // Шаг от границы окна, после кот. квадрат будет двигаться
    private static final int BORDER_DIFF = 70;
    // Пикселей на шаг
    private static final int PX_PER_STEP = 10;
    // Текущая позиция квадрата
    private int posX, posY;

    SquarePanel() {
        posX = getWidth() / 2;
        posY = getHeight() / 2;
        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                final int curX = e.getX();
                final int curY = e.getY();
                if (isCursorOverBorder(curX, curY)) {
                    animateSquare(curX, curY);
                }
            }
        });
        setFocusable(true);
    }

    private boolean isCursorOverBorder(final int curX, final int curY) {
        final int borderSizeX = getWidth() - BORDER_DIFF;
        final int borderSizeY = getHeight() - BORDER_DIFF;

        return curX <= BORDER_DIFF || curY <= BORDER_DIFF
            || curX >= borderSizeX || curY >= borderSizeY;
    }

    private void animateSquare(final int toX, final int toY) {
        final int aX = toX - posX;
        final int aY = toY - posY;
        final int stepNumber = (int) (Math.sqrt(aX * aX + aY * aY) / PX_PER_STEP) + 1;
        final int stepX = aX / stepNumber;
        final int stepY = aY / stepNumber;
        for (int i = 0; i < stepNumber; i++) {
            posX += stepX;
            posY += stepY;
            repaint();
        }
    }

    @Override
    public void paintComponent(Graphics h) {
        super.paintComponent(h);
        Graphics2D g = (Graphics2D) h;
        g.drawRect(posX, posY, SQUARE_SIZE, SQUARE_SIZE);
    }
}
