package com.stadnik.javafifthsemester;

/*
 * 7. Длина дуги окружности изменяется нажатием клавиш от 1 до 9.
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Arc {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            ArcFrame frame = new ArcFrame();
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}

class ArcFrame extends JFrame {
    protected final static int DEFAULT_WIDTH  = 400;
    protected final static int DEFAULT_HEIGHT = 400;

    ArcFrame() {
        setTitle("Arc changing example");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        getContentPane().add(new ArcPanel());
    }
}

class ArcPanel extends JPanel {
    private int angleStart = 0, angleEnd = 0;
    private static final int ARC_XPOS = 50;
    private static final int ARC_YPOS = 50;
    private static final int ARC_WIDTH = 200;
    private static final int ARC_HEIGHT = 200;

    ArcPanel() {
        addKeyListener(new ArcKeyListener());
        setFocusable(true);
    }

    private void resizeArc(final int startAngle, final int endAngle) {
        this.angleStart = startAngle;
        this.angleEnd = endAngle;
        repaint();
    }

    private class ArcKeyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent event) {
            final int keyCode = event.getKeyCode();
            switch (keyCode) {
                case KeyEvent.VK_1:
                    resizeArc(80, 20);
                    break;
                case KeyEvent.VK_2:
                    resizeArc(70, 40);
                    break;
                case KeyEvent.VK_3:
                    resizeArc(60, 60);
                    break;
                case KeyEvent.VK_4:
                    resizeArc(50, 80);
                    break;
                case KeyEvent.VK_5:
                    resizeArc(40, 100);
                    break;
                case KeyEvent.VK_6:
                    resizeArc(30, 120);
                    break;
                case KeyEvent.VK_7:
                    resizeArc(20, 140);
                    break;
                case KeyEvent.VK_8:
                    resizeArc(10, 160);
                    break;
                case KeyEvent.VK_9:
                    resizeArc(0, 180);
                    break;
            }
        }
    }

    @Override
    public void paintComponent(Graphics h) {
        super.paintComponent(h);
        Graphics2D g = (Graphics2D) h;
        g.drawArc(ARC_XPOS, ARC_YPOS, ARC_WIDTH, ARC_HEIGHT, angleStart, angleEnd);
    }
}
