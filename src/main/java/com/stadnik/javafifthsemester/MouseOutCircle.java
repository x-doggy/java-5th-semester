package com.stadnik.javafifthsemester;

/*
 * 9. Окружность "убегает" от указателя мыши.
 * При приближении на некоторое расстояние окружность появляется
 * в другом месте апплета.
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MouseOutCircle {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            MouseOutFrame frame = new MouseOutFrame();
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}

class MouseOutFrame extends JFrame {
    protected final static int DEFAULT_WIDTH  = 600;
    protected final static int DEFAULT_HEIGHT = 600;

    MouseOutFrame() {
        setTitle("Окружность \"убегает\" от указателя мыши");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        getContentPane().add(new MouseOutPanel());
    }
}

class MouseOutPanel extends JPanel {
    private static final int RADIUS = 65;
    private int x = getWidth() / 2;
    private int y = getHeight() / 2;

    MouseOutPanel() {
        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                final int overlapRadius = RADIUS + 30;
                if (isCursorHovered(e.getX(), e.getY(), overlapRadius)) {
                    x = (int) (Math.random() * (getWidth()  - overlapRadius));
                    y = (int) (Math.random() * (getHeight() - overlapRadius));
                }
                repaint();
            }
        });
        setFocusable(true);
    }

    private boolean isCursorHovered(int pos_x, int pos_y, int rad) {
        return Math.abs(pos_x - x) <= rad && Math.abs(pos_y - y) <= rad;
    }

    @Override
    public void paintComponent(Graphics h) {
        super.paintComponent(h);
        Graphics2D g = (Graphics2D) h;
        g.setStroke(new BasicStroke(4));
        g.setColor(new Color(25, 25, 25));
        g.drawOval(x, y, RADIUS, RADIUS);
    }
}
