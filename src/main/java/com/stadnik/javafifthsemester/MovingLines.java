package com.stadnik.javafifthsemester;

/*
 * 3. По экрану движутся одна за одной строки из массива строк.
 * Направление движения и значение каждой строки выбирается случайным образом.
 */

import java.awt.*;
import java.util.Random;
import javax.swing.*;

public class MovingLines {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            LinesFrame frame = new LinesFrame();
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}

class LinesFrame extends JFrame {
    protected final static int DEFAULT_WIDTH  = 800;
    protected final static int DEFAULT_HEIGHT = 400;

    LinesFrame() {
        setTitle("Двигающиеся строки");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

        Container contentPane = getContentPane();
        LinesPanel linesPanel = new LinesPanel();
        contentPane.add(linesPanel, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel();

        JButton startButton = new JButton("Start");
        JButton stopButton = new JButton("Stop");
        JButton closeButton = new JButton("Close");

        startButton.addActionListener(e -> {
            startButton.setEnabled(false);
            stopButton.setEnabled(true);
            linesPanel.resumeThread();
        });
        startButton.setEnabled(false);

        stopButton.addActionListener(e -> {
            stopButton.setEnabled(false);
            startButton.setEnabled(true);
            linesPanel.suspendThread();
        });

        closeButton.addActionListener(e -> System.exit(0));

        buttonPanel.add(startButton);
        buttonPanel.add(stopButton);
        buttonPanel.add(closeButton);

        contentPane.add(buttonPanel, BorderLayout.SOUTH);
    }
}

class LinesPanel extends JPanel implements Runnable {
    private int posX = 0, posY = 0, y = 20;
    private static int STEP = 15;
    private static final int KOEF = 10;
    private static final String[] STRINGS = {
            "Hello!",
            "How are you?",
            "What's up?",
            "Do you learn Java?"
    };
    private String str = STRINGS[(int) (Math.random() * (STRINGS.length - 1))];
    private Thread thread;

    LinesPanel() {
        resumeThread();
        setFocusable(true);
    }

    void suspendThread() {
        thread = null;
    }

    final void resumeThread() {
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        Random rnd = new Random();
        while (thread == Thread.currentThread()) {
            try {
                Thread.sleep(100);
                repaint();
                posY += STEP;
                if (posY >= getHeight()) {
                    posX = (int) (Math.random() * getWidth());
                    str = STRINGS[(int) (Math.random() * (STRINGS.length - 1))];
                    if (rnd.nextInt(2) == 0) {
                        posY = 0;
                        STEP = KOEF;
                    } else {
                        STEP = -KOEF;
                    }
                }
                if (posY < 0) {
                    posX = (int) (Math.random() * getWidth());
                    if (rnd.nextInt(2) == 0) {
                        posY = 0;
                        STEP = KOEF;
                    } else {
                        posY = getHeight();
                        STEP = -KOEF;
                    }
                    str = STRINGS[(int) (Math.random() * (STRINGS.length - 1))];
                    repaint();
                }
            } catch (InterruptedException e) {
            }
        }
    }

    @Override
    public void paintComponent(Graphics h) {
        super.paintComponents(h);
        Graphics2D g = (Graphics2D) h;
        g.drawString(str, posX, posY);
    }
}
